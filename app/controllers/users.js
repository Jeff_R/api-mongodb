const UserModel = require('../models/user.js');

module.exports = class Users {
    constructor(app, connect) {
        this.app = app;
        this.UserModel = connect.model('User', UserModel);
        this.run();
    }

    run() {
        this.app.post('/users/', (req, res)=> {
            try{
                const userModel = new this.UserModel(req.body);

                userModel.save().then((user) => {
                    res.status(200).json(user || {});
                }).catch((err)=>{
                    console.log(err);   
                    res.status(200).json({});
                });
            } catch(err) {
                console.error(`[ERROR] post:users -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: 'Bad Request'
                });
            }
        });

        this.app.get('/users/:email',async (req, res)=> {
            try{
                this.UserModel.findOne({ email: req.params.email }).then((user) => {
                    res.status(200).json(user || 'User not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] post:users -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });

        this.app.get('/users/tok/:token',async (req, res)=> {
            try{
                console.log(req.params.token);
                this.UserModel.findOne({ token: req.params.token }).then((user) => {
                    res.status(200).json(user || 'User not found');
                }).catch((err)=>{  
                    res.status(422).json(err.message || {});
                });
            } catch(err) {
                console.error(`[ERROR] post:users -> ${err}`);
            
                res.status(400).json({
                    code: 400,
                    message: `${err}`
                });
            }
        });

    }
}