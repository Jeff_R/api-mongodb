const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');

const Schema = new mongoose.Schema({
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    age: {
        type: Number,
    },
    city: {
        type: String,
    },
    email: {
        type: String,
        unique: true,
        match: [/^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,3})+$/, 'Please fill a valid email address']
    },
    password: {
        type: String,
        required: [true, 'Password is required'],
        minlength: [6, 'Password must be more than 6 characters']
    },
    dob: Date,
    coverPicture: {
        type: String,
        default: "",
    },
    profilePicture: {
        type: String,
        default: "",
    },
    token:{ 
        type: String, 
        default: GUID.value 
    }
}, {
    collection:'users',
    // Bien optimiser la requête
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Données qu'on récupère
        ret.id = ret._id;

        delete ret._id;
    }
});

module.exports = Schema;