const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Group name is required']
    },
    description: {
        type: String,
        required: [true, 'Group description is required']
    },
    icone: String,
    coverPicture: String,
    type: {
        type: String,
        enum:['Public','Private', 'Secret'],
        required: true
    },
    members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    organisateurs: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
}, {
    collection:'groups',
    // Optimize the query well
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        // doc : Data we collect
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;
