const mongoose = require('mongoose');
const GUID = require('mongoose-guid')(mongoose);
const uniqueValidator = require('mongoose-unique-validator');
const UserModel = require('./user.js');

const Schema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'event name is required']
    },
    description: {
        type: String,
        required: [true, 'Event description is required']
    },
    beginDate:{
        type: Date,
        default: Date.now
    },
    endDate: Date,
    location: String,
    coverPicture: String,
    visibility: {
        type: String,
        enum:['Private','Public'],
        required: true
    },
    managers:[{ type: mongoose.Schema.Types.ObjectId, ref: 'User', required:true }],
    participants: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
}, {
    collection:'events',
    minimize: false,
    versionKey: false
}).set('toJSON',{
    transform: (doc, ret) => {
        ret.id = ret._id;

        delete ret._id;
    }
});

Schema.plugin(uniqueValidator, { message: '{PATH} already exist' });

module.exports = Schema;