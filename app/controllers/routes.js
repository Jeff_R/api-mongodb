const Users = require('./users.js');
const Events = require('./events.js');
const Groups = require('./groups.js');

module.exports = {
    Users,
    Events
};